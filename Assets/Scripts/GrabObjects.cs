﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabObjects : MonoBehaviour
{
	public float throw_speed = 2.0f;	// How fast you throw
	private SteamVR_TrackedObject tracked_object;
	private bool holding_object = false;	// if already holding object


	private SteamVR_Controller.Device device
	{
		get { return SteamVR_Controller.Input((int)tracked_object.index); }
	}


	private void Awake()
	{
		 tracked_object = GetComponent<SteamVR_TrackedObject>();
	}


	private void OnTriggerStay(Collider collider)
	{
		if (collider.tag == "Grabbable" || collider.tag == "Ball")
		{
			Rigidbody rigidbody = collider.gameObject.GetComponent<Rigidbody>();
			
			// Hold object when trigger is down
			if( device.GetPressDown(SteamVR_Controller.ButtonMask.Trigger) && !holding_object )
			{
				rigidbody.isKinematic = true;
				collider.gameObject.transform.SetParent(this.transform);
				holding_object = true;
			}

			// Let go and throw object when let go
			if( device.GetPressUp(SteamVR_Controller.ButtonMask.Trigger) && holding_object )
			{
				// If it's a balltool, mark it as thrown
				if(collider.gameObject.tag == "Ball")
					collider.GetComponent<Ball>().OnThrow();

				rigidbody.isKinematic = false;
				collider.gameObject.transform.SetParent(null);
				ThrowObject(collider.attachedRigidbody);
				holding_object = false;
			}
		}
	}


	private void ThrowObject(Rigidbody rigidbody)
	{
		rigidbody.velocity = device.velocity * throw_speed;
		rigidbody.angularVelocity = device.angularVelocity;
	}
}
