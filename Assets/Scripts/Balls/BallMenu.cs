﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class BallMenu : MonoBehaviour
{
	public GameObject player;
	public List<GameObject> balls = new List<GameObject>();
	// public List<float> delays = new List<float>();
	private float ball_size = 0.1f;
	private float ball_offset = 0.2f;

	private void Start()
	{
		// Set variables
		foreach( GameObject ball in balls)
		{
			ball.GetComponent<Ball>().player = player;
		}
		Debug.Log(balls[0]);

		// Place balls
		float width = (balls.Count * ball_size) + ((balls.Count-1) * ball_offset);
		float last_pos = -(width/2);
		Debug.Log(width);

		foreach(GameObject ball in balls)
		{
			Vector3 ball_pos = new Vector3(transform.position.x, transform.position.y, transform.position.z) + (-transform.right * last_pos);
			Instantiate(ball, ball_pos, transform.rotation, this.transform);
			last_pos = last_pos + ball_size + ball_offset;
		}
	}
}
