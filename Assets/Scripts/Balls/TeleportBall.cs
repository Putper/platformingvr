﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportBall : MonoBehaviour
{
	private Ball ball;
	private GameObject player;


	private void Awake()
	{
		ball = GetComponent<Ball>();
		player = ball.player;
	}


	private void OnCollisionEnter(Collision collision)
	{
		// Get height of touched object
		float width = collision.gameObject.GetComponent<MeshRenderer>().bounds.extents.x * 2;
		float height = collision.gameObject.GetComponent<MeshRenderer>().bounds.extents.y * 2;
		float depth = collision.gameObject.GetComponent<MeshRenderer>().bounds.extents.z * 2;

		// If ball is thrown on top of it
		if(this.transform.position.y >= collision.gameObject.transform.position.y + (height/2))
		{
			// if player fits on top
			if(width >= 2.0f && depth >= 1.5f || width >= 1.5f && depth >= 2.0f)
			{
				// Set player on top
				Vector3 new_pos = this.transform.position;
				new_pos.y = collision.gameObject.transform.position.y + (height/2);
				player.transform.position = new_pos;
				Destroy(this.gameObject);
			}
		}

		// if(collision.gameObject.tag == "Teleportable")
		// {
		// 	// Get point relative to collider
		// 	Vector3 local_dir = collision.transform.InverseTransformPoint( collision.contacts[0].point ).normalized;

		// 	// Point relative to collider
		// 	float x_dot = Vector3.Dot(local_dir, Vector3.right);	// If positive, it's right of the collider. If negative; left.
		// 	float y_dot = Vector3.Dot(local_dir, Vector3.up);	// If positive, it's above the collider. If negative; under.
		// 	float z_dot = Vector3.Dot(local_dir, Vector3.forward);	// If positive, it's in front of the collider. If negative; behind.

		// 	// we use this to check which direction is pointing the furthest
		// 	float x_power = Mathf.Abs(x_dot);
		// 	float y_power = Mathf.Abs(y_dot);
		// 	float z_power = Mathf.Abs(z_dot);

		// 	// If the collision is below or above
		// 	if(y_power > x_power && y_power > z_power)
		// 	{
		// 		// If it's above
		// 		if(y_dot > 0)
		// 		{
		// 			player.transform.position = this.transform.position;
		// 			if(ball.has_been_thrown) Destroy(this.gameObject);
		// 		}
		// 	}

		// }
	}
}
