﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMenuSpawner : MonoBehaviour
{
    public GameObject player;
    public float forward_offset = 0.1f;
    public float height_offset = 0.05f;
    public GameObject ball_menu;
    public GameObject controller_model;
	private SteamVR_TrackedObject tracked_object;
    private GameObject instantiated_ball_menu;


	private SteamVR_Controller.Device device
	{
		get { return SteamVR_Controller.Input((int)tracked_object.index); }
	}


	private void Awake()
	{
		 tracked_object = GetComponent<SteamVR_TrackedObject>();
         ball_menu.GetComponent<BallMenu>().player = player;
	}

	
	private void Update()
	{
        if( device.GetPressDown(SteamVR_Controller.ButtonMask.Grip) )
        {
            Vector3 menu_pos = transform.position + (transform.forward*forward_offset) + (transform.up * height_offset);
            instantiated_ball_menu = Instantiate(ball_menu, menu_pos, transform.rotation, controller_model.transform);
        }

        if( device.GetPressUp(SteamVR_Controller.ButtonMask.Grip) )
        {
            Destroy(instantiated_ball_menu);
        }
	}
}
